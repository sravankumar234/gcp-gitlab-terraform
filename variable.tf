

variable "project" {
  type        = string
  description = "Name of the project"
}

variable "location" {
  type        = string
  description = "Name of the location"
}