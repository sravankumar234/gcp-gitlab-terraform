provider "google" {
    credentials = "${file("./credential.json")}"
}

provider "google-beta" {
    credentials = "${file("./credential.json")}"
}